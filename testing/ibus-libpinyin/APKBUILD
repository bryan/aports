# Contributor: Peng Huang <shawn.p.huang@gmail.com>
# Contributor: BYVoid <byvoid1@gmail.com>
# Contributor: Peng Wu <alexepico@gmail.com>
# Maintainer: Ziyao <ziyao@disroot.org>
pkgname=ibus-libpinyin
pkgver=1.15.1
pkgrel=0
pkgdesc="Intelligent Pinyin engine based on libpinyin for IBus"
url="https://github.com/libpinyin/ibus-libpinyin"
# armhf: no libpinyin
arch="all !armhf"
license="GPL-3.0-only"
makedepends="
	ibus-dev
	libpinyin-dev
	sqlite-dev
	"
subpackages="$pkgname-lang"
source="https://github.com/libpinyin/ibus-libpinyin/releases/download/$pkgver/ibus-libpinyin-$pkgver.tar.gz"
options="!check" # there are none

build() {
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	./configure \
		--host=$CHOST \
		--build=$CBUILD \
		--prefix=/usr \
		--libexecdir=/usr/lib/ibus
	make
}

package() {
	make install DESTDIR=$pkgdir

	# remove duplicate python bytecode
	find "$pkgdir" -name "*.opt-1.pyc" -delete
}

sha512sums="
c11bb1a022d36f1622bca408f2cfd153be1dd1faa514a43f8bc2bdade37712d201ea1c6b53e6c5f8ea35909cd2f46112b637425620b29f81247b84bf7ef93018  ibus-libpinyin-1.15.1.tar.gz
"
