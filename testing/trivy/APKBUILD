# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=trivy
pkgver=0.38.2
pkgrel=0
pkgdesc="Simple and comprehensive vulnerability scanner for containers"
url="https://github.com/aquasecurity/trivy"
arch="all"
# s390x: tests SIGSEGV: https://github.com/aquasecurity/trivy/issues/430
arch="$arch !s390x"
license="Apache-2.0"
makedepends="btrfs-progs-dev go linux-headers lvm2-dev"
source="https://github.com/aquasecurity/trivy/archive/v$pkgver/trivy-$pkgver.tar.gz"
options="net !check" # needs tinygo to turn go into wasm for tests

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	make build VERSION=$pkgver
}

check() {
	make test
}

package() {
	install -Dm755 $pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
570475e797cb8e752b2c45660b62dcf4a914020024da25aa3667541af6b5afc283ef473671ad459aba8c91f0d19c6c2885594a0f196b9dbc161308c4e2a3924a  trivy-0.38.2.tar.gz
"
